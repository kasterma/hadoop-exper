#!/usr/bin/env python
#
# generate some year number data to find the max in.

import random

outfilename = "input/data.txt"
outfile = open(outfilename, "w")
answerfilename = "answers.txt"
answerfile = open(answerfilename, "w")

years = range(1900,2000)
nos_per_year = 1000

for year in years:
    temps = [random.randint(0,200*nos_per_year) for idx in range(0,nos_per_year)]
    for temp in temps:
        outfile.write("{year} {temp}\n".format(year=year, temp=temp))
    answerfile.write("{year}\t{max}\n".format(year=year, max=max(temps)))
        
outfile.close()
answerfile.close()
