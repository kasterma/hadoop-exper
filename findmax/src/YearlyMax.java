package net.kasterma;
 
import java.io.IOException;
import java.util.*;
 
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapred.*;
import org.apache.hadoop.util.*;
 
public class YearlyMax {
 
    public static class Map 
        extends MapReduceBase 
        implements Mapper<LongWritable, Text, Text, IntWritable> {

        private final static IntWritable one = new IntWritable(1);
        private Text year = new Text();
        private String prev_year = new String("no year");
        private int max = -1;
 
        public void map(LongWritable key,
                        Text value,
                        OutputCollector<Text, IntWritable> output,
                        Reporter reporter) throws IOException {
            String[] line = value.toString().split(" ");
            year.set(line[0]);
            if (!prev_year.equals(line[0])) {
                max = -1;
                prev_year = new String(line[0]);
            }
            int thisNum = Integer.parseInt(line[1]);
            if (thisNum > max) {
                // by only outputting nums bigger the output is drastically smaller
                // in one run reduced from 100000 to 731
                max = thisNum;
                output.collect(year, new IntWritable(max));
            }
        }
    }
 
    public static class Reduce
        extends MapReduceBase
        implements Reducer<Text, IntWritable, Text, IntWritable> {

        public void reduce(Text key,
                           Iterator<IntWritable> values,
                           OutputCollector<Text, IntWritable> output,
                           Reporter reporter) throws IOException {
            int max = -1;
            int next;
            while (values.hasNext()) {
                next = values.next().get();
                if (next > max) {
                    max = next;
                }
            }
            output.collect(key, new IntWritable(max));
        }
    }
 
    public static void main(String[] args) throws Exception {
        JobConf conf = new JobConf(YearlyMax.class);
        conf.setJobName("yearly max");
 
        conf.setOutputKeyClass(Text.class);
        conf.setOutputValueClass(IntWritable.class);
 
        conf.setMapperClass(Map.class);
        conf.setCombinerClass(Reduce.class);
        conf.setReducerClass(Reduce.class);
 
        conf.setInputFormat(TextInputFormat.class);
        conf.setOutputFormat(TextOutputFormat.class);
 
        FileInputFormat.setInputPaths(conf, new Path(args[0]));
        FileOutputFormat.setOutputPath(conf, new Path(args[1]));
 
        JobClient.runJob(conf);
    }
}