input_starts = ['A', 'B', 'C']
def input_filename(input_start):
    return "input/" + input_start + "input.txt"

def open_input(input_start):
    return open(input_filename(input_start), "w")

NUM=1000
base_range = range(0,NUM)

cont = []
for ins in input_starts:
    of = open_input(ins)
    cont.extend([ ins + str(idx) for idx in base_range])
    for item in cont:
        of.write(item + "\n")
    of.close()
