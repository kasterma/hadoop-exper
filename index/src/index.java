package net.kasterma;
 
import java.io.IOException;
import java.util.*;
 
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapred.*;
import org.apache.hadoop.util.*;
import org.apache.hadoop.io.WritableComparator;
 
public class index {
 
    public static class Map 
        extends MapReduceBase 
        implements Mapper<LongWritable, Text, Text, Text> {

        private Text filename;
        private Text word = new Text();

        public void configure(JobConf job) {
            filename = new Text(job.get("map.input.file"));
        }

        public void map(LongWritable key,
                        Text value,
                        OutputCollector<Text, Text> output,
                        Reporter reporter) throws IOException {
            String line = value.toString();
            StringTokenizer tokenizer = new StringTokenizer(line);
            while (tokenizer.hasMoreTokens()) {
                word.set(tokenizer.nextToken());
                output.collect(word, filename);
            }
        }
    }
 
    public static class Reduce
        extends MapReduceBase
        implements Reducer<Text, Text, Text, Text> {

        private String value;
        private String filenames;
        private Text outval = new Text();

        public void reduce(Text key,
                           Iterator<Text> values,
                           OutputCollector<Text, Text> output,
                           Reporter reporter) throws IOException {

            filenames = "";
            while (values.hasNext()) {
                value = values.next().toString();
                filenames += "," + value;
            }
            outval.set(filenames);
            output.collect(key, outval);
        }
    }
 

    public static void main(String[] args) throws Exception {
        JobConf conf = new JobConf(index.class);
        conf.setJobName("create index");
 
        conf.setOutputKeyClass(Text.class);
        conf.setOutputValueClass(Text.class);
 
        conf.setMapperClass(Map.class);
        conf.setReducerClass(Reduce.class);

        conf.setInputFormat(TextInputFormat.class);
        conf.setOutputFormat(TextOutputFormat.class);
 
        FileInputFormat.setInputPaths(conf, new Path(args[0]));
        FileOutputFormat.setOutputPath(conf, new Path(args[1]));
 
        JobClient.runJob(conf);
    }
}