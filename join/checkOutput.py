import sys

outputfile = open("output/part-00000", "r").readlines()

for line in outputfile:
    row = line.split(", ")
    if (int(row[2]) != int(row[3][1:])):
        print "Values do not match"
        sys.exit(1)
 
print "output/part-0000 checks out"
