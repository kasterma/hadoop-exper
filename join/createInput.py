Afile = open("input/Arecords.txt", "w")
Bfile = open("input/Brecords.txt", "w")

NUM=1000000

for idx in range(1, NUM):
    Afile.write("A, " + str(idx) + ", A" + str(idx) + "\n")

for idx in range(3, 3*NUM):
    Bfile.write("B, " + str(idx) + ", " + str(idx/ 3) + "\n")

Afile.close()
Bfile.close()
