package net.kasterma;
 
import java.io.IOException;
import java.util.*;
 
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapred.*;
import org.apache.hadoop.util.*;
 
public class join {
 
    public static class Map 
        extends MapReduceBase 
        implements Mapper<LongWritable, Text, Text, Text> {

        private final static IntWritable one = new IntWritable(1);
        private Text out_key = new Text();
        private Text out_value = new Text();
        private String line;
        private String [] row;
        private String zeros = "00000000";

        public void map(LongWritable key,
                        Text value,
                        OutputCollector<Text, Text> output,
                        Reporter reporter) throws IOException {
            line = value.toString();
            row = line.split(", ");

            if (row[0].equals("A")) {
                out_key.set(zeros.substring(0, 8-(row[1].length())) +
                            row[1] + row[0]);
                out_value.set(line);
                output.collect(out_key, out_value);
            } else {  // row is a B record
                out_key.set(zeros.substring(0, 8-(row[2].length())) + 
                            row[2] + row[0]);
                out_value.set(line);
                output.collect(out_key, out_value);
            }
        }
    }
 
    public static class Reduce
        extends MapReduceBase
        implements Reducer<Text, Text, Text, Text> {

        private String line;
        private String keystring;
        private String[] row;
        private String Aval = "NOT YET SET!!!";
        private Boolean Aval_set = Boolean.FALSE;
        private Boolean Bloop_run = Boolean.FALSE;
        private Text out_key = new Text("ignore");
        private Text out_value = new Text();

        public void reduce(Text key,
                           Iterator<Text> values,
                           OutputCollector<Text, Text> output,
                           Reporter reporter) throws IOException {
            keystring = key.toString();
            if (keystring.charAt(8) == 'A') {
                row = values.next().toString().split(", ");
                Aval = new String(row[2]);
                Aval_set = Boolean.TRUE;
                if (!Bloop_run) {
                    reporter.setStatus("AAAAAAAAAAAAAAA before B: as should be");
                }
            } else {
                if (!Aval_set) {
                    reporter.setStatus("BBBBBBBBBBBBBBBB before got A");
                    // set to be easily recognizable in output
                }
                while (values.hasNext()) {
                    out_value.set(values.next() + ", " + Aval);
                    output.collect(key, out_value);
                }
                Bloop_run = Boolean.TRUE;
            }

        }
    }
 

    public static class bkPartitioner
        implements Partitioner<Text ,Text> {

        private String keystring;
        private String numstring;

        public bkPartitioner () {
        }

        @Override
        public int getPartition(Text key, Text value, int numPartitions) {
            keystring = key.toString();
            numstring = keystring.substring(0, keystring.length() - 1);
            return(Integer.valueOf(numstring) % numPartitions);
        }

        @Override
        public void configure(JobConf arg0) {
 
        }

    }
            

    public static void main(String[] args) throws Exception {
        JobConf conf = new JobConf(join.class);
        conf.setJobName("join A and B records");
 
        conf.setOutputKeyClass(Text.class);
        conf.setOutputValueClass(Text.class);
 
        conf.setMapperClass(Map.class);
        conf.setReducerClass(Reduce.class);

        conf.setNumReduceTasks(2);

        // without this partitioner there is no guarantee that
        // the A record and the B records it needs to be joined with
        // will end up at the same reducer.  For the current input
        // it does in fact not work out (part-00000 misses an A
        // record for the first couple of B records).
        conf.setPartitionerClass(bkPartitioner.class);

        conf.setInputFormat(TextInputFormat.class);
        conf.setOutputFormat(TextOutputFormat.class);
 
        FileInputFormat.setInputPaths(conf, new Path(args[0]));
        FileOutputFormat.setOutputPath(conf, new Path(args[1]));
 
        JobClient.runJob(conf);
    }
}