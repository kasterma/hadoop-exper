import random

for idx in range(0, 10):
    outfile = open("input/inputA" + str(idx) + ".txt", "w")
    As = ["A" for i in range(0, 5 + idx)]
    Bs = ["B" for i in range(0,20)]
    total = []
    total.extend(As)
    total.extend(Bs)
    random.shuffle(total)
    for it in total:
        outfile.write(it + "\n")
    outfile.close()

for idx in range(0, 10):
    outfile = open("input/inputB" + str(idx) + ".txt", "w")
    Bs = ["B" for i in range(0,20)]
    total = []
    total.extend(Bs)
    random.shuffle(total)
    for it in total:
        outfile.write(it + "\n")
    outfile.close()

