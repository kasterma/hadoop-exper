package net.kasterma;

import java.io.IOException;
import java.util.*;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapred.*;
import org.apache.hadoop.util.*;

public class search {

    public static class Map 
        extends MapReduceBase 
        implements Mapper<LongWritable, Text, Text, IntWritable> {

        private Text filename;
        private IntWritable one = new IntWritable(1);

        public void configure(JobConf job) {
            filename = new Text(job.get("map.input.file"));
        }

        public void map(LongWritable key,
                        Text value,
                        OutputCollector<Text, IntWritable> output,
                        Reporter reporter) throws IOException {

            if (value.toString().equals("A")) {
                output.collect(filename, one);
            }

        }
    }

    public static class Reduce
        extends MapReduceBase
        implements Reducer<Text, IntWritable, Text, IntWritable> {

        private int count;
        private IntWritable curint = new IntWritable();

        public void reduce(Text key,
                           Iterator<IntWritable> values,
                           OutputCollector<Text, IntWritable> output,
                           Reporter reporter) throws IOException {

            count = 0;

            while (values.hasNext()) {
                count += values.next().get();
            }

            curint.set(count);
            output.collect(key, curint);

        }
    }

    public static void main(String[] args) throws Exception {
        JobConf conf = new JobConf(search.class);
        conf.setJobName("search");
 
        conf.setOutputKeyClass(Text.class);
        conf.setOutputValueClass(IntWritable.class);
 
        conf.setMapperClass(Map.class);
        conf.setReducerClass(Reduce.class);

        conf.setInputFormat(TextInputFormat.class);
        conf.setOutputFormat(TextOutputFormat.class);
 
        FileInputFormat.setInputPaths(conf, new Path(args[0]));
        FileOutputFormat.setOutputPath(conf, new Path(args[1]));
 
        JobClient.runJob(conf);
    }

}