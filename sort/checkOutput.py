## only check the first part at this point

import sys

def checkfile(filename, initletters):
    cfile = open(filename, "r").readlines()
    # least letter and number
    prev_letter = "A"
    prev_number = 0
    for line in cfile:
        item = line.split("\t")[0]
        letter = item[0]
        if not letter in initletters:
            print "wrong initletters in " + filename
            print line
            sys.exit(1)

        number = int(item[1:])
        if not (number >= prev_number or letter > prev_letter):
            print "Values not sorted in " + filename
            sys.exit(1)
        prev_letter = letter
        prev_number = number

checkfile("output/part-00000", ["A", "B"])
checkfile("output/part-00001", ["C", "D"])
