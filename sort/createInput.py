import random

outfile = open("input/input.txt", "w")

NUM=100

base_range = range(0,NUM)

Arange = ["A" + str(idx) for idx in base_range]
Brange = ["B" + str(idx) for idx in base_range]
Crange = ["C" + str(idx) for idx in base_range]
Drange = ["D" + str(idx) for idx in base_range]
total = []
total.extend(Arange)
total.extend(Brange)
total.extend(Crange)
total.extend(Drange)
random.shuffle(total)

for item in total:
    outfile.write(item + "\n")

outfile.close()
