package net.kasterma;

// import java.io.IOException;
import java.io.*;
import java.util.*;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapred.*;
import org.apache.hadoop.util.*;


public class bkText implements WritableComparable {
    
    private String str1;
    private String str2;

    private char letter1;
    private char letter2;

    private int val1;
    private int val2;

    private String value;

    public bkText() {
    }

    public bkText(String tt) {
        value = tt;
    }

    public bkText(Text tt) {
        value = tt.toString();
    }

    public void readFields(DataInput in) throws IOException {
        value = new String();
        letter1 = in.readChar();
        while (letter1 != '\n') {
            value = value + (new Character(letter1)).toString();
            letter1 = in.readChar();
        }
    }

    public void write(DataOutput out) throws IOException {
        out.writeChars(value);
        out.writeChar('\n');
    }

    public String toString() {
        return value;
    }

    public Text toText() {
        return new Text(value);
    }

    public int compareTo(Object other) {

        if (!(other instanceof bkText)) {
            return -1;
        }

        bkText t2 = (bkText) other;

        str1 = value;
        str2 = t2.toString();

        letter1 = str1.charAt(0);
        letter2 = str2.charAt(1);

        val1 = Integer.parseInt(str1.substring(1));
        val2 = Integer.parseInt(str2.substring(1));

        if (val1 < val2) {
            return -1;
        } else if (val2 < val1) {
            return 1;
        }
        // integer values are equal


        if (letter1 < letter2) {
            return -1;
        } else if (letter2 < letter1) {
            return 1;
        }
        // initial letters are equal too

        return 0;
    }

    public boolean equals(Object other) {
        if (!(other instanceof bkText)) {
            return false;
        }

        bkText t2 = (bkText) other;

        return value.equals(t2.value);
    }
}
