package net.kasterma;
 
import java.io.IOException;
import java.util.*;
 
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapred.*;
import org.apache.hadoop.util.*;
import org.apache.hadoop.io.WritableComparator;
 
public class sort {
 
    public static class Map 
        extends MapReduceBase 
        implements Mapper<LongWritable, Text, bkText, bkText> {

        private bkText out_value;

        public void map(LongWritable key,
                        Text value,
                        OutputCollector<bkText, bkText> output,
                        Reporter reporter) throws IOException {

            out_value = new bkText(value);
            output.collect(out_value, out_value);

        }
    }
 
    public static class Reduce
        extends MapReduceBase
        implements Reducer<bkText, bkText, Text, Text> {

        private Text value;

        public void reduce(bkText key,
                           Iterator<bkText> values,
                           OutputCollector<Text, Text> output,
                           Reporter reporter) throws IOException {

            while (values.hasNext()) {
                value = values.next().toText();
                output.collect(value, value);
            }

        }
    }
 

    // very specific to this sorting problem
    // splitting in A,B in one and C,D in the other is an
    // even split
    // with this partitioner concatenating the two output
    // files if they are independently sorted results in a
    // sorted file
    public static class firstLetterPartitioner
        implements Partitioner<bkText ,bkText> {

        private char letter;

        @Override
        public int getPartition(bkText key, bkText value, int numPartitions) {
            letter = key.toString().charAt(0);
            
            if (letter == 'A' || letter == 'B') {
                return 0;
            } else {
                return 1;
            }

        }

        @Override
        public void configure(JobConf arg0) {
 
        }

    }
    

    public static void main(String[] args) throws Exception {
        JobConf conf = new JobConf(sort.class);
        conf.setJobName("sort: all A,B's in one and C,B's the other");
 
        conf.setOutputKeyClass(bkText.class);
        conf.setOutputValueClass(bkText.class);
 
        conf.setMapperClass(Map.class);
        conf.setReducerClass(Reduce.class);

        conf.setNumReduceTasks(2);

        conf.setPartitionerClass(firstLetterPartitioner.class);

        //conf.setOutputKeyComparatorClass(bkText.class);

        conf.setInputFormat(TextInputFormat.class);
        conf.setOutputFormat(TextOutputFormat.class);
 
        FileInputFormat.setInputPaths(conf, new Path(args[0]));
        FileOutputFormat.setOutputPath(conf, new Path(args[1]));
 
        JobClient.runJob(conf);
    }
}