input_starts = ['A', 'B', 'C']
def input_filename(input_start):
    return "input/" + input_start + "input.txt"

def open_input(input_start):
    return open(input_filename(input_start), "w")

NUM=1000
base_range = range(0,NUM)

cont = {}
cont['A'] = ['onceA', 'twiceAonceB twiceAonceB', 'thriceA thriceA thriceA', 'in_all']
cont['B'] = ['in_all', 'twiceAonceB']
cont['C'] = ['in_all', 'Conly']

# ('A', 'onceA', (* (/ 1.0 7.0) (/ (log10 (/ 3.0 1.0)) (log10 2.71))) 0.15742497373892908
# ('A', 'twiceAonceB', (* (/ 2.0 7.0) (/ (log10 (/ 3.0 2.0)) (log10 2.71))) 0.11620174770366679
# ('A', 'in_all', (* (/ 1.0 7.0) (/ (log10 (/ 3.0 3.0)) (log10 2.71))) 0.0
# ('B', 'in_all', (* (/ 1.0 2.0) (log10 (/ 3.0 3.0))) 0.0
# ('B', 'twiceAonceB'), (* (/ 1.0 2.0) (/ (log10 (/ 3.0 2.0)) (log10 2.71)))0.20335305848141688
# ('C', 'in_all', (* (/ 1.0 2.0) (log10 (/ 3.0 3.0))) 0.0
# ('C', 'Conly', (* (/ 1.0 2.0) (/ (log10 (/ 3.0 1.0)) (log10 2.71)))0.5509874080862518



for ins in input_starts:
    of = open_input(ins)
    for item in cont[ins]:
        of.write(item + "\n")
    of.close()
