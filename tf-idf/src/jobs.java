package net.kasterma;
 
import java.io.IOException;
import java.util.*;
 
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapred.*;
import org.apache.hadoop.util.*;
import org.apache.hadoop.io.WritableComparator;
import java.lang.Math.*;
 
public class jobs {
 
    public static class MapPerDocPerWord
        extends MapReduceBase 
        implements Mapper<LongWritable, Text, Text, IntWritable> {

        private String filename;
        private Text wordfilename = new Text();
        private IntWritable one = new IntWritable(1);

        public void configure(JobConf job) {
            filename = job.get("map.input.file");
        }

        public void map(LongWritable key,
                        Text value,
                        OutputCollector<Text, IntWritable> output,
                        Reporter reporter) throws IOException {
            String line = value.toString();
            StringTokenizer tokenizer = new StringTokenizer(line);
            while (tokenizer.hasMoreTokens()) {
                wordfilename.set(tokenizer.nextToken() + " " + filename);
                output.collect(wordfilename, one);
            }
        }
    }
 
    public static class ReducePerDocPerWord
        extends MapReduceBase
        implements Reducer<Text, IntWritable, Text, IntWritable> {

        private String value;
        private String filenames;
        private IntWritable outval = new IntWritable();
        private int count;

        public void reduce(Text key,
                           Iterator<IntWritable> values,
                           OutputCollector<Text, IntWritable> output,
                           Reporter reporter) throws IOException {

            count = 0;
            while (values.hasNext()) {
                count += values.next().get();
            }
            outval.set(count);
            output.collect(key, outval);
        }
    }
 
    public static class MapWordCountDoc
        extends MapReduceBase 
        implements Mapper<LongWritable, Text, Text, Text> {

        private String [] line;
        private String word;
        private String filename;
        private Text filenameText = new Text();
        private String count;
        private Text outval = new Text();

        public void map(LongWritable key,
                        Text value,
                        OutputCollector<Text, Text> output,
                        Reporter reporter) throws IOException {
            line = value.toString().split("\\s");
            word = line[0];
            filename = line[1];
            count = line[2];
            outval.set(word + " " + count);
            filenameText.set(filename);
            output.collect(filenameText, outval);
        }
    }
 
    public static class ReduceWordCountDoc
        extends MapReduceBase
        implements Reducer<Text, Text, Text, Text> {

        private String [] value;
        private String word;
        private int count;
        private ArrayList<String []> valCopies = new ArrayList();
        private Iterator<String []> valCopyIter;
        private Text outval = new Text();
        private Text outkey = new Text();

        public void reduce(Text key,
                           Iterator<Text> values,
                           OutputCollector<Text, Text> output,
                           Reporter reporter) throws IOException {

            // add the counts per word for a given document to get the total
            valCopies.clear();
            count = 0;
            while (values.hasNext()) {
                value = values.next().toString().split(" ");
                valCopies.add(value);
                count += Integer.parseInt(value[1]);
            }

            // construct the outputs
            valCopyIter = valCopies.iterator();

            while (valCopyIter.hasNext()) {
                value = valCopyIter.next();
                outval.set(value[1] + " " + count);
                outkey.set(value[0] + " " + key.toString());
                output.collect(outkey, outval);
            }
        }
    }

    public static class MapWordCountCorpus
        extends MapReduceBase 
        implements Mapper<LongWritable, Text, Text, Text> {

        private String [] line;
        private String word;
        private String filename;
        private String count1;
        private String count2;
        private Text outkey = new Text();
        private Text outval = new Text();

        public void map(LongWritable key,
                        Text value,
                        OutputCollector<Text, Text> output,
                        Reporter reporter) throws IOException {
            line = value.toString().split("\\s");
            word = line[0];
            filename = line[1];
            count1 = line[2];
            count2 = line[3];
            outkey.set(word);
            outval.set(filename + " " + count1 + " " + count2 + " 1");
            output.collect(outkey, outval);
        }
    }
 
    public static class ReduceWordCountCorpus
        extends MapReduceBase
        implements Reducer<Text, Text, Text, Text> {

        private String [] value;
        private String word;
        private int count;
        private ArrayList<String []> valCopies = new ArrayList();
        private Iterator<String []> valCopyIter;
        private Text outval = new Text();
        private Text outkey = new Text();

        public void reduce(Text key,
                           Iterator<Text> values,
                           OutputCollector<Text, Text> output,
                           Reporter reporter) throws IOException {

            // add the counts per word for a given document to get the total
            valCopies.clear();
            count = 0;
            while (values.hasNext()) {
                value = values.next().toString().split("\\s");
                valCopies.add(value);
                count += Integer.parseInt(value[3]);
            }

            // construct the outputs
            valCopyIter = valCopies.iterator();

            while (valCopyIter.hasNext()) {
                value = valCopyIter.next();
                outval.set(value[1] + " " + value[2] + " " + count);
                outkey.set(key.toString() + " " + value[0]);
                output.collect(outkey, outval);
            }
        }
    }



    public static class MapCompute
        extends MapReduceBase 
        implements Mapper<LongWritable, Text, Text, Text> {

        private String [] line;
        private String word;
        private String filename;
        private int n, N, m;
        private int D = 3;            // total documents in corpus is 3
        private double tfidf;
        private Text outkey = new Text();
        private Text outval = new Text();

        public void map(LongWritable key,
                        Text value,
                        OutputCollector<Text, Text> output,
                        Reporter reporter) throws IOException {
            line = value.toString().split("\\s");
            word = line[0];
            filename = line[1];
            n = Integer.parseInt(line[2]);
            N = Integer.parseInt(line[3]);
            m = Integer.parseInt(line[4]);
            tfidf = ((double) n/(double) N) * Math.log((double) 3/ (double) m);
            outkey.set(word + " " + filename);
            outval.set("" + tfidf);
            output.collect(outkey, outval);
        }
    }
 
    public static class ReduceIdentity
        extends MapReduceBase
        implements Reducer<Text, Text, Text, Text> {

        public void reduce(Text key,
                           Iterator<Text> values,
                           OutputCollector<Text, Text> output,
                           Reporter reporter) throws IOException {

            output.collect(key, values.next());
        }
    }

    public static void main(String[] args) throws Exception {
        JobConf conf = new JobConf(jobs.class);
        if (args[0].equals("job1")) {
            conf.setJobName("word count per doc");
 
            conf.setOutputKeyClass(Text.class);
            conf.setOutputValueClass(IntWritable.class);
 
            conf.setMapperClass(MapPerDocPerWord.class);
            conf.setReducerClass(ReducePerDocPerWord.class);

            conf.setInputFormat(TextInputFormat.class);
            conf.setOutputFormat(TextOutputFormat.class);

            FileInputFormat.setInputPaths(conf, new Path(args[1]));
            FileOutputFormat.setOutputPath(conf, new Path(args[2]));
 
            JobClient.runJob(conf);
        } else if (args[0].equals("job2")) {
            conf.setJobName("word count per doc");
 
            conf.setOutputKeyClass(Text.class);
            conf.setOutputValueClass(Text.class);
 
            conf.setMapperClass(MapWordCountDoc.class);
            conf.setReducerClass(ReduceWordCountDoc.class);

            conf.setInputFormat(TextInputFormat.class);
            conf.setOutputFormat(TextOutputFormat.class);

            FileInputFormat.setInputPaths(conf, new Path(args[1]));
            FileOutputFormat.setOutputPath(conf, new Path(args[2]));
 
            JobClient.runJob(conf);
        } else if (args[0].equals("job3")) {
            conf.setJobName("word count in corpus");
 
            conf.setOutputKeyClass(Text.class);
            conf.setOutputValueClass(Text.class);
 
            conf.setMapperClass(MapWordCountCorpus.class);
            conf.setReducerClass(ReduceWordCountCorpus.class);

            conf.setInputFormat(TextInputFormat.class);
            conf.setOutputFormat(TextOutputFormat.class);

            FileInputFormat.setInputPaths(conf, new Path(args[1]));
            FileOutputFormat.setOutputPath(conf, new Path(args[2]));
 
            JobClient.runJob(conf);
        } else if (args[0].equals("job4")) {
            conf.setJobName("final compute pass");
 
            conf.setOutputKeyClass(Text.class);
            conf.setOutputValueClass(Text.class);
 
            conf.setMapperClass(MapCompute.class);
            conf.setReducerClass(ReduceIdentity.class);

            conf.setInputFormat(TextInputFormat.class);
            conf.setOutputFormat(TextOutputFormat.class);

            FileInputFormat.setInputPaths(conf, new Path(args[1]));
            FileOutputFormat.setOutputPath(conf, new Path(args[2]));
 
            JobClient.runJob(conf);
        } else {
            System.out.println("NOT A VALID JOB\n");
        }
 
    }
}